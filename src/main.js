import Vue from 'vue'
import App from './App.vue'
import "@/assets/css/tailwind.css"
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import router from "@/router"
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import { MdCard , MdButton, MdIcon, MdSteppers, MdChips, MdDivider, MdSubheader, MdMenu, MdProgress} from 'vue-material/dist/components'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'

Vue.use(MdCard);
Vue.use(MdButton);
Vue.use(MdIcon);
Vue.use(MdSteppers);
Vue.use(MdChips);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(MdDivider);
Vue.use(MdSubheader);
Vue.use(MdMenu);
Vue.use(MdProgress);

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
