import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/Home'
import Projects from '@/views/Projects'
import Experience from '@/views/Experience'
import Skills from '@/views/Skills'

Vue.use(Router)


export default new Router({
    mode: 'hash',
    routes:[
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/projects',
            name: 'projects',
            component: Projects
        },
        {
            path: '/experience',
            name: 'experience',
            component: Experience
        },
        {
            path: '/skills',
            name: 'skills',
            component: Skills
        }
    ]
})